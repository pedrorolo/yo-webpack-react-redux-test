const redux = require('redux');
const reducers = require('../reducers');

module.exports = function(initialState) {
  let debugEnhancer = window.devToolsExtension ? window.devToolsExtension() : f => f
  const store = redux.createStore(reducers, initialState, debugEnhancer);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers')
      store.replaceReducer(nextReducer)
    })
  }

  return store
}
