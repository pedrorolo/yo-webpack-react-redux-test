'use strict';

import React from 'react';

require('styles//Todos.scss');

let TodosComponent = (props) => {
  return (
    <ul className="todos-component">
    {
      props.todos.map(function(t){
        return (<li key={t.id} className="todo">{t.text}</li>);
      })
    }
    </ul>
  );
}


// Uncomment properties you need
// TodosComponent.propTypes = {};
// TodosComponent.defaultProps = {};

export default TodosComponent;
