require('normalize.css');
require('styles/App.css');

import React from 'react';
import AddTodo from '../containers/AddTodo.js';
import TodosComponent from 'components/TodosComponent.js';


//let yeomanImage = require('../images/yeoman.png');

class AppComponent extends React.Component {
  render() {
    return (
      <div className="index">
        <AddTodo />
        <TodosComponent todos={this.props.Todos} />
      </div>
    );
  }
}

AppComponent.defaultProps = {
};

export default AppComponent;
