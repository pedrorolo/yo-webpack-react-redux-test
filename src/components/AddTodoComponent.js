'use strict';

import React from 'react';

require('styles//AddTodo.scss');

let AddTodoComponent = (props) => {
  return (
    <form className="addtodo-component"
      onSubmit={  event => {
          event.preventDefault();
          props.onSubmit(props.value)
        }
      }>
      <input type='text'
             value={props.value}
             onChange={(e) => {
                 props.onChange(e.target.value)
               }
             } />
      <button type='submit'>Save</button>
    </form>
  );
}


export default AddTodoComponent;
