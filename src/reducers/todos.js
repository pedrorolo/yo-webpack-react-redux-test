/* Define your initial state here.
 *
 * If you change the type from object to something else, do not forget to update
 * src/container/App.js accordingly.
 */
const initialState = [];

module.exports = function(state = initialState, action) {
  /* Keep the reducer clean - do not mutate the original state. */
  switch(action.type) {
    case 'ADD_TODO': {
      return [...state, action.todo];
    } break;
    default: {
      /* Return original state if no actions were consumed. */
      return state;
    }
  }
}
