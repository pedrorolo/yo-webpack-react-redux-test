import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import AddTodoComponent from '../components/AddTodoComponent';

class AddTodo extends Component {
  render() {
    return <AddTodoComponent {...this.props}/>;
  }
}

AddTodo.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = {value: state.AddTodo};
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    onSubmit: require('../actions/AddTodo.js'),
    onChange: require('../actions/ChangeAddTodo.js')
  };
  const actionMap = bindActionCreators(actions, dispatch);
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTodo);
