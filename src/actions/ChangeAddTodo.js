module.exports = function(newValue) {
  return { type: 'CHANGE_ADD_TODO', newValue: newValue };
};
