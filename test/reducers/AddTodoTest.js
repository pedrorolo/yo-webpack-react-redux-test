var reducer = require('../../src/reducers/AddTodo');

describe('AddTodo', () => {

  it('should not change the passed state', (done) => {

    const state = Object.freeze({});
    reducer(state, {type: 'INVALID'});

    done();
  });
});
